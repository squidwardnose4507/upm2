use std::path::PathBuf;

use log::{LevelFilter, info};
use simplelog::{ColorChoice, CombinedLogger, TermLogger, TerminalMode};
use structopt::StructOpt;
use ucore::paths;
use upk::{package, Configuration, Error};

pub(crate) mod config;
use config::Config;

const PROGRAM_NAME: &str = "upk";

#[derive(Debug, Clone, PartialEq, StructOpt)]
enum BuildSubcommand {
    /// Verify that a packagespec is valid
    Verify {
        /// The packagespec to verify
        #[structopt(parse(from_os_str), default_value = "upkspec.toml")]
        path: PathBuf,
    },

    /// Build a package from a packagespec
    Build {
        /// The packagespec to build
        #[structopt(parse(from_os_str), default_value = "upkspec.toml")]
        path: PathBuf,

        #[structopt(short, long)]
        verbose: bool,
    },
}

#[derive(Debug, Clone, PartialEq, StructOpt)]
#[structopt(name = PROGRAM_NAME, about = "Tool for inspecting and building upk packages.")]
#[structopt()]
enum Options {
    Inspect,

    /// Build or verify a Package spec
    Spec(BuildSubcommand),
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    log_init();
    let options = Options::from_args();
    let config = get_config().await.unwrap_or_default();

    match options {
        Options::Spec(subcommand) => match subcommand {
            BuildSubcommand::Verify { path } => {
                verify(path).await?;
            }
            BuildSubcommand::Build { path, verbose } => {}
        },
        Options::Inspect => {}
    }
    Ok(())
}

async fn verify(path: PathBuf) -> Result<(), anyhow::Error> {
    let packagespec = package::Spec::from_file(&path).await?;
    info!(
        "Loaded packagespec successfully from \"{}\",",
        path.display()
    );

    if packagespec.sources.len() != 0 {
        info!("Downloading sources...");
    }

    for source in packagespec.sources {
        
    }
    Ok(())
}

/// Try to parse a `Config` from a configuration file in the `paths::CONFIG_DIR` directory.
///
/// # Errors
///
/// Returns errors if:
///
/// - `paths::CONFIG_DIR` is None
/// - An IO error or parsing error is returned from `Config::from_file`
async fn get_config() -> Result<Config, anyhow::Error> {
    // Get configuration directory and append the file we want.
    let configuration_dir = paths::CONFIG_DIR.as_ref().ok_or(Error::MissingConfigDir)?;
    let config_file = configuration_dir.join(format!("{PROGRAM_NAME}.toml"));
    Ok(Config::from_file(&config_file).await?)
}

fn log_init() {
    let log_config = simplelog::ConfigBuilder::new()
        .set_time_level(LevelFilter::Off)
        .build();
    TermLogger::init(
        LevelFilter::Warn,
        log_config,
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )
    .unwrap();
}
