use std::{path::PathBuf, sync::Arc};

use async_trait::async_trait;
use rand::{distributions::Alphanumeric, Rng};
use ring::digest::{self, Algorithm, SHA256};
use tokio::{fs, task};

pub mod package;

pub mod error;
pub use error::Error;
pub(crate) use error::Result;
use serde::Deserialize;

#[async_trait]
pub trait Configuration: Sized + for<'a> Deserialize<'a> {
    /// Try to parse a TOML configuration (`Self`) from `path`
    async fn from_file(path: impl Into<PathBuf> + std::marker::Send) -> Result<Self>
    where
        Self: Send + 'static,
    {
        let path: PathBuf = path.into();
        let contents = tokio::fs::read_to_string(&path)
            .await
            .map_err(|e| Error::Io(e, path.clone()))?;
        tokio::task::spawn_blocking(move || {
            toml::from_str::<Self>(&contents).map_err(|e| Error::InvalidPackageSpec(e, path))
        })
        .await
        .unwrap()
    }
}

async fn clone_git_repo(git: &String, out: PathBuf) -> Result<PathBuf> {
    let tmpdir = ucore::paths::TEMP_DIR
        .as_ref()
        .ok_or(Error::MissingTempDir)?;

    // If the temporary directory doesn't exist, attempt to create it now.
    if !tmpdir.exists() {
        fs::create_dir_all(tmpdir)
            .await
            .map_err(|e| Error::Io(e, tmpdir.clone()))?;
    }
    let tmp_path = tmpdir.join(out);

    if git.starts_with("http") {
        let repo = git2::Repository::clone(git, tmp_path).map_err(Error::Git)?;
    } else {
        return Err(Error::Unhandled(
            "Can't Figure out how to clone git repo".to_string(),
        ));
    }

    todo!()
}

fn random_alphanum16() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(16)
        .map(char::from)
        .collect::<String>()
}

async fn check_hash(a: &'static Algorithm, data: Arc<bytes::Bytes>, sum: &String) -> Result<()> {
    let hash = task::spawn_blocking({
        let data = data.clone();
        move || String::from_utf8(digest::digest(a, &data).as_ref().to_vec()).unwrap()
    })
    .await
    .unwrap();

    if hash != *sum {
        return Err(Error::DissimilarHash {
            expected: sum.to_owned(),
            found: hash,
        });
    }

    Ok(())
}
