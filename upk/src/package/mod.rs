use std::{path::PathBuf, sync::Arc};

use git2::Repository;
use serde::{Deserialize, Serialize};
use tokio::{fs, task};

use crate::{Configuration, Error, Result};

pub mod archive;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Sources {
    /// An HTTP(S) source
    Http {
        /// The HTTP(S) URI
        http: String,
        /// A SHA256 checksum for a file found at `http`
        sha256sum: Option<String>,
        /// A SHA512 checksum for a file found at `http`
        sha512sum: Option<String>,
        /// Output path
        out: Option<PathBuf>,
    },

    /// A Git repository
    Git {
        /// The Git repository to pull from
        git: String,
        /// Output path
        out: Option<PathBuf>,
    },
}

impl Sources {
    pub async fn get(&self, prefix: PathBuf) -> Result<PathBuf> {
        use ring::digest::{self, SHA256, SHA512};

        match self {
            Sources::Http {
                http,
                sha256sum,
                sha512sum,
                out,
            } => {
                let response = reqwest::get(http).await.map_err(Error::HttpRequest)?;

                let path = {
                    let p = {
                        if let Some(out) = out {
                            out.to_owned()
                        } else {
                            let path_base = response
                                .url()
                                .path_segments()
                                .ok_or(Error::Unhandled(String::from("Invalid source URL")))?
                                .filter(|x| !x.is_empty())
                                .last()
                                .unwrap_or("");

                            PathBuf::from(path_base)
                        }
                    };
                    prefix.join(p)
                };

                let data = response.bytes().await.map_err(Error::HttpRequest)?;
                let data_arc = Arc::new(data);
                

                if let Some(sum) = sha256sum {
                    crate::check_hash(&SHA256, data_arc.clone(), sum).await?;
                }

                if let Some(sum) = sha512sum {
                    crate::check_hash(&SHA512, data_arc.clone(), sum).await?;
                }

                // If the file's parent doesn't exist, create it.
                let ppath = path.parent().unwrap_or(&path);
                if !ppath.exists() {
                    fs::create_dir_all(ppath)
                        .await
                        .map_err(|e| Error::Io(e, ppath.to_path_buf()))?;
                }

                // Save the data to the file
                fs::write(&path, data_arc.as_ref())
                    .await
                    .map_err(|e| Error::Io(e, path.clone()))?;

                Ok(path)
            }
            Sources::Git { git, out } => {
                let out = out.to_owned().unwrap_or_else(|| {
                    PathBuf::from(format!("{}.git", crate::random_alphanum16()))
                });
                crate::clone_git_repo(git, out).await
            }
        }

    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Build {
    build_scripts: Vec<PathBuf>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Install {
    /// File installtion locations handled by the package installer
    Default {
        sources: Vec<PathBuf>,
        kind: DefaultInstallKind,
    },
    /// File installation locations set here
    Custom { custom: Vec<CustomInstall> },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[repr(u8)]
pub enum DefaultInstallKind {
    Binary,
    Documentation,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct CustomInstall {
    /// Source
    from: PathBuf,
    /// Destination in the installed system
    to: PathBuf,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Spec {
    /// The person(s), or entity(s) maintaining the package
    pub maintainers: Vec<String>,
    /// The name of the package
    pub name: String,
    /// The version of the program being packaged
    pub version: String,
    /// The version of the package specification (To signal it should be rebuilt)
    pub package_version: u32,
    /// Sources to download before building anything
    pub sources: Vec<Sources>,
    /// Build scripts to run to prepare the sourced software for packaging
    pub build: Option<Build>,
    /// Files to install
    pub install: Vec<Install>,
}

impl Configuration for Spec {}
