use std::{path::PathBuf, fs::Permissions};

use async_trait::async_trait;
use tokio::fs;

use crate::{Result, Error};

#[derive(Debug, Clone, PartialEq)]
pub struct Package {
    maintainer: String,

}

#[derive(Debug, Clone, PartialEq)]
pub struct InstallJob {
    from: PathBuf,
    to: PathBuf,
    permissions: Permissions,
}

// impl Default for InstallJob {
//     fn default() -> Self {
//         Self { from: Default::default(), to: Default::default(), permissions: }
//     }
// }

#[async_trait]
impl Job for InstallJob {
    async fn perform(&self) -> Result<()> {
        // Copy file
        fs::copy(&self.from, &self.to).await.map_err(|e| Error::Io(e, self.to.to_owned()))?;
        log::info!("Copied: \"{}\" -> \"{}\"", self.from.display(), self.to.display());

        // Set permissions
        // fs::set_permissions(&self.to, self.permissions).await.map_err(|e| Error::Io(e, self.to.to_owned()));
        log::info!("Set permissons: \"{}\"", self.to.display());

        Ok(())
    }
}

#[async_trait]
pub trait Job {
    async fn perform(&self) -> Result<()>;
}