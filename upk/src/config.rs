use serde::{Deserialize, Serialize};

use crate::Configuration;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(untagged)]
pub(crate) enum ParallelDownloadPreferences {
    Auto(bool),
    Custom(u8),
}

impl std::default::Default for ParallelDownloadPreferences {
    fn default() -> Self {
        Self::Auto(true)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Config {
    /// Wether to download files in parallel and to what extent to parallelize.
    parallel_download: ParallelDownloadPreferences,
}

impl Configuration for Config {}