use std::path::PathBuf;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Unhandled: {0}")]
    Unhandled(String),

    #[error("I/O: \"{1}\": {0}")]
    Io(std::io::Error, PathBuf),

    #[error("Invalid PackageSpec: \"{1}\": {0}")]
    InvalidPackageSpec(toml::de::Error, PathBuf),

    #[error("Couldn't find an existing configuration file and couldn't create one!")]
    MissingConfigDir,

    #[error("Couldn't find anywhere to store temporary files!")]
    MissingTempDir,

    #[error("HttpRequest: {0}")]
    HttpRequest(reqwest::Error),

    #[error("Git error: {0}")]
    Git(git2::Error),

    #[error(
        "Dissimilar hash error: Hashes didn't match.\nexpected: \"{expected}\"\nfound: \"{found}\""
    )]
    DissimilarHash { expected: String, found: String },
}

pub(crate) type Result<T> = std::result::Result<T, Error>;
