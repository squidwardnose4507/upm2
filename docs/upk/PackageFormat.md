# Package Format

Packages are `xz` compressed `tar` archives. They have a directory structure
like the following.

```
├── Package.ron
└── src
    └── bash
```

Packages should be generated from a file with contents that look like the following:

```toml
maintainers = ["<NAME>"]
name = "<PACKAGE_NAME>"
version = "<SOFTWARE_VERSION>"
packageVersion = "<PACKAGE_VERSION>"

[[source]]
http = "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/snapshot/linux-6.3-rc7.tar.gz"
sha256sum = "<CHECKSUM>"

[[source]]
git = "https://github.com/example/test"
branch = "my-branch" # Optional
out = "<OUTPUT_FILE_NAME>" # Optional

[[source]]
git = "https://github.com/example/test"
tag = "v0.0.0" # Optional
out = "<OUTPUT_FILE_NAME>" # Optional

[[source]]
git = "https://github.com/example/test"
out = "<OUTPUT_FILE_NAME>" # Optional

[[source]]
inPath = "./file.sh"
sha256sum = "<CHECKSUM>"
out = "" # Optional

[build]
# Environment variables containing package information should be available to the script.
buildScripts = ["./<PathToShellScript>"]

[[install]]
sources = ["<path>"]
kind = "binary"

[[install]]
sources = ["<path3>", "<Path2>", "<Path4>"]
kind = "doc"


# Destination paths are relative to package directory ($PACKAGE_DIR).
[[install]]
custom = [
    {from = "./file", to = "/usr/bin/file"},
    {from = "./file_other", to = "/usr/local/bin/file_other"}
]
```