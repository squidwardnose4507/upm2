use std::{fmt, str::FromStr};

use libc::{S_IRGRP, S_IROTH, S_IRUSR, S_IWGRP, S_IWOTH, S_IWUSR, S_IXGRP, S_IXOTH, S_IXUSR};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Permissions(u32);

impl From<u32> for Permissions {
    fn from(mut value: u32) -> Self {
        //     let mut digits = [0u8; 3];

        //     for i in 0..3 {
        //         digits[i] = (value % 10) as u8;
        //         value /= 10;

        //         if i == 0 {
        //             break;
        //         }
        //     }
        //     digits.reverse();
        //     let mut x: u32 = digits[0] as u32;
        //     x

        Self(value)
    }
}

impl Permissions {
    pub const DEFAULT: u32 =
        Self::WRITE_USER | Self::READ_USER | Self::READ_GROUP | Self::READ_OTHER;
    const READ_USER: u32 = S_IRUSR;
    const WRITE_USER: u32 = S_IWUSR;
    const EXECUTE_USER: u32 = S_IXUSR;
    const READ_GROUP: u32 = S_IRGRP;
    const WRITE_GROUP: u32 = S_IWGRP;
    const EXECUTE_GROUP: u32 = S_IXGRP;
    const READ_OTHER: u32 = S_IROTH;
    const WRITE_OTHER: u32 = S_IWOTH;
    const EXECUTE_OTHER: u32 = S_IXOTH;

    /// Input should look like: "rwxrw-r--"
    pub fn from_str(s: &str) -> Option<Self> {
        if s.len() != 9 {
            return None;
        }
        let mut perms = 0;
        for (i, c) in s.chars().enumerate() {
            match c {
                'r' if i / 3 == 0 => perms |= Self::READ_USER,
                'w' if i / 3 == 0 => perms |= Self::WRITE_USER,
                'x' if i / 3 == 0 => perms |= Self::EXECUTE_USER,

                'r' if i / 3 == 1 => perms |= Self::READ_GROUP,
                'w' if i / 3 == 1 => perms |= Self::WRITE_GROUP,
                'x' if i / 3 == 1 => perms |= Self::EXECUTE_GROUP,

                'r' if i / 3 == 2 => perms |= Self::READ_OTHER,
                'w' if i / 3 == 2 => perms |= Self::WRITE_OTHER,
                'x' if i / 3 == 2 => perms |= Self::EXECUTE_OTHER,
                '-' => (),
                _ => return None,
            }
        }

        Some(Self(perms))
    }
}

impl fmt::Display for Permissions {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        const PERMISSION_CHARS: [char; 3] = ['r', 'w', 'x'];
        // Initialize all permission feilds to `-`
        let mut permissions = ['-'; 9];

        // Loop nine times
        for i in 0..9 {
            // If the bit field isn't zero
            if self.0 & (1 << (8 - i)) != 0 {
                // We modulo 3 so we don't have to repeat the `rwx` charaters three times in the array.
                permissions[i] = PERMISSION_CHARS[i % 3];
            }
        }
        // Write out the `permissions` character array.
        write!(f, "{}", permissions.iter().collect::<String>())
    }
}

impl Default for Permissions {
    fn default() -> Self {
        Self(Self::DEFAULT)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_permissions_display() {
        const PERMS: Permissions =
            Permissions(Permissions::READ_USER | Permissions::WRITE_USER | Permissions::READ_GROUP);
        const EXPECTED: &str = "rw-r-----";

        assert_eq!(PERMS.to_string(), EXPECTED);
    }
    
    #[test]
fn test_permissions_from_str() {
    let valid_chars = ['-', 'r', 'w', 'x'];

    for i in 0..512 {
        let perm_str = format!("{:09b}", i)
            .chars()
            .map(|c| if c == '1' { 'r' } else { '-' })
            .collect::<String>();

        let mut expected = String::new();
        let mut perm_chars = perm_str.chars();
        expected.push(perm_chars.next().unwrap_or('-'));
        expected.push(perm_chars.next().unwrap_or('-'));
        expected.push(perm_chars.next().unwrap_or('-'));
        expected.push(perm_chars.next().unwrap_or('-'));
        expected.push(perm_chars.next().unwrap_or('-'));
        expected.push(perm_chars.next().unwrap_or('-'));
        expected.push(perm_chars.next().unwrap_or('-'));
        expected.push(perm_chars.next().unwrap_or('-'));
        expected.push(perm_chars.next().unwrap_or('-'));

        if perm_str.chars().all(|c| valid_chars.contains(&c)) {
            assert_eq!(Permissions::from_str(&perm_str).unwrap().to_string(), expected);
        } else {
            assert!(Permissions::from_str(&perm_str).is_none());
        }
    }
}

    
    
    
}
