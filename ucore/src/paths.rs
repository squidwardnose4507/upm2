use std::path::PathBuf;

use directories::ProjectDirs;
use lazy_static::lazy_static;

lazy_static! {
    pub(crate) static ref PROJECT_DIRS: Option<ProjectDirs> =
        ProjectDirs::from("com.github", "El-Wumbus", super::PACKAGE_MANAGER_NAME);
    pub static ref CONFIG_DIR: Option<PathBuf> = {
        let mut dirs = vec![
            PROJECT_DIRS
                .as_ref()
                .and_then(|x| Some(x.config_dir().to_path_buf())),
            fallbacks::CONFIG_DIR.and_then(|x| Some(PathBuf::from(x))),
        ]
        .into_iter()
        .filter_map(|x| x);

        let mut perferred = dirs
            .clone()
            .filter_map(|x| if x.is_dir() { Some(x) } else { None });

        let Some(dir) = perferred.next() else {return dirs.next()};
        Some(dir)
    };
    pub static ref TEMP_DIR: Option<PathBuf> = {
        let mut dirs = vec![
            fallbacks::TEMP_DIR.and_then(|x| Some(PathBuf::from(x))),
            PROJECT_DIRS
                .as_ref()
                .and_then(|x| Some(x.cache_dir().to_path_buf())),
            Some(PathBuf::from("./.upm"))
        ]
        .into_iter()
        .filter_map(|x| x);
        dirs.next()
    };
}

#[cfg(target_os = "linux")]
mod fallbacks {
    pub(crate) const CONFIG_DIR: Option<&str> = Some("/etc/upm.d");
    pub(crate) const TEMP_DIR: Option<&str> = Some("/tmp/upm");
}

#[cfg(not(target_os = "linux"))]
mod fallbacks {
    pub(crate) const CONFIG_DIR: Option<&str> = None;
    pub(crate) const TEMP_DIR: Option<&str> = None;
}
